import { Component, OnInit, Input } from '@angular/core';
import {ApiService} from '../api.service'

@Component({
  selector: 'app-chat2',
  templateUrl: './chat2.component.html',
  styleUrls: ['./chat2.component.css']
})
export class Chat2Component implements OnInit {

  constructor(private ApiService:ApiService) { }
 
  chat2;
@Input() chat;
  ngOnInit() {
    this.chat2=this.ApiService.getMessages();
    
  }
  

}
