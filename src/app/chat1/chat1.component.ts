import { Component, OnInit, Input } from '@angular/core';
import {ApiService} from '../api.service';

@Component({
  selector: 'app-chat1',
  templateUrl: './chat1.component.html',
  styleUrls: ['./chat1.component.css']
})
export class Chat1Component implements OnInit {

  constructor(private ApiService:ApiService) { }
  
  
@Input() chat1;
chat;
  ngOnInit() {
    
    this.chat=this.ApiService.getMessages();console.log(this.chat);
    
  }

}
