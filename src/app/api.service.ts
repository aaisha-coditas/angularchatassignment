import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor() { }
  chat=[];
  chatSec=[];

  getMessages(){
    return this.chat.slice();
    
  }
  getSecChat(){
    return this.chatSec.slice();
  }
}
