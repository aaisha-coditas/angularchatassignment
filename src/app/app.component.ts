import { Component, OnInit } from '@angular/core';
import {ApiService} from './api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'chat';
  constructor(private ApiService:ApiService){

  }
  message1:string;
  message2:string;

chat;
chatSec;
  
  getMessage1(){
   this.ApiService.chat.push(this.message1);
   this.ApiService.chat.push(this.message2);
   this.chat=this.ApiService.getMessages();
   
   
  }
  getMessage2(){
    
    
    this.ApiService.chatSec.push(this.message2);
    this.ApiService.chatSec.push(this.message1);
    this.chatSec=this.ApiService.getSecChat();
    }
    ngOnInit(){
     

    }
}
